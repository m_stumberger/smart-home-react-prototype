import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from "./configureStore";
// import { BrowserRouter, Route } from 'react-router-dom'

import MainApp from './components/Dashboard/Dashboard';
import './index.css';


const App = () => (
    <Provider store={configureStore()}>
        {/*<BrowserRouter>*/}
            {/*<div>*/}
                {/*<Route path="/" component={ MainApp } />*/}
                {/*/!*<Route path="/hello" component={ Hello } />*!/*/}
                {/*/!*<Route path="/bye" component={ Bye } />*!/*/}
            {/*</div>*/}
        {/*</BrowserRouter>*/}
        <MainApp />
    </Provider>
);

const rootEl = document.getElementById('root');
ReactDOM.render(<App />, rootEl);
