import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import reduxAutobahn from 'redux-autobahn-js';
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import { Box } from 'reactjs-admin-lte';

import { Dashboard, Header } from 'react-adminlte-dash';

import { SideBar } from './containers/Sidebar/Sidebar'

const nav = () => ([
    <Header.Item href="/some/link" key="1" />
]);

const isConnected = props => {
  return props.autobahn.connection.isConnected;
};

const isSubscribed = (props, topic) => {
  return props.autobahn.subscriptions.filter(s => s.topic === topic).length > 0;
};

class Hello extends React.Component{
    render() { return <div>Hello</div> }
}

class Bye extends React.Component{
    render() { return <div>Bye</div> }
}

class MainApp extends Component {
  componentWillMount() {
    console.log('open connection');
    this.props.actions.openConnection();
  }

  componentWillReceiveProps(newProps) {
    if (this.isNewConnection(newProps)) {
      console.log('connected');
      this.props.actions.subscribe('com.example.oncounter');
      // this.props.actions.call();

      // this.props.actions.subscribe('BTC_XMR');
    }

    if (this.isNewSubscription(newProps, 'com.example.oncounter')) {
      console.log('com.example.oncounter');
    }

  }

  isNewConnection(newProps) {
    return !isConnected(this.props) && isConnected(newProps);
  }

  isNewSubscription(newProps, topic) {
    return !isSubscribed(this.props, topic) && isSubscribed(newProps, topic);
  }

  constructor(props){
    super(props);

    console.log(props);

    this.state = { response: 'com.example.add2'};

    this.onInputChange = this.onInputChange.bind(this);
    this.onButtonPress = this.onButtonPress.bind(this);
  }

  onInputChange(event){
    console.log(event.target.value);
    this.setState({ response: event.target.value });

  };

  onButtonPress(event){
      event.preventDefault();
      console.log("Button was pressed!");
      this.props.actions.call(this.state.response, [4,2])
  }

  render() {
      console.log(this.state.result);
      return <div>
          <Dashboard
              navbarChildren={nav()}
              sidebarChildren={SideBar()}
              theme="skin-blue">
              <div>
                  <form onSubmit={this.onButtonPress} className="input-group">
                      <input placeholder="Response"
                             className="form-control"
                             value={this.state.response}
                             onChange={this.onInputChange}
                      />
                      <span className="input-group-btn">
                    <button type="submit" className="btn btn-secundary">Press me</button>
                </span>
                  </form>
              </div>
              <div>
                  <Box>
                      <Box.Header>
                          <Box.Title>Responses: </Box.Title>
                      </Box.Header>
                      <Box.Body>
                          <li>RPC: {JSON.stringify(this.props.MainApp.rpc)}</li>
                          <li>PUB/SUB: {JSON.stringify(this.props.MainApp.pubsub)}</li>
                      </Box.Body>
                  </Box>
              </div>
              <BrowserRouter>
                  <div>
                      <Switch>
                          <Route path="/hello" component={ Hello } />
                          <Route path="/bye" component={ Bye } />
                      </Switch>
                  </div>
              </BrowserRouter>
          </Dashboard>
      </div>;
  }
}

const mapStateToProps = state => state;

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(reduxAutobahn.actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainApp);
