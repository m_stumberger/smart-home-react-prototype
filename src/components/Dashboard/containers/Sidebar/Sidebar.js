import React from 'react';
import { Link } from 'react-router-dom'
import { Sidebar } from 'react-adminlte-dash'



export const SideBar = () => ([
    <Sidebar.Menu header="NAVIGATION" key="1">

            <Sidebar.Menu.Item title="Home" />

            <Sidebar.Menu.Item title="Hello" href="/hello" />

            <Sidebar.Menu.Item title="Bye" href="/" />
    </Sidebar.Menu>
]);